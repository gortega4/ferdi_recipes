# Recipe for Telegram version K

First, locate the file webview.js
For example:

```
find ~/.config/ -name "telegram" -type d -exec echo "Path: {}/webview.js" \;
```

Next, you must update the content and then reload your telegram service.

## Note: I strongly recommend that you make a backup copy of your original file, in case you have any previous custom code.